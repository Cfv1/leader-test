import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import {InputFieldComponent} from './input-field.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    InputFieldComponent
  ],

  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],

  exports: [
    InputFieldComponent
  ]
})

export class InputFieldModule { }

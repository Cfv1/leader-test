import {InputFieldComponent} from './input-field.component';
import {TestBed, ComponentFixture} from '@angular/core/testing';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

describe('InputFieldComponent', () => {

  let component: InputFieldComponent;
  let fixture: ComponentFixture<InputFieldComponent>;
  beforeEach(() => {

    // refine the test module by declaring the test component
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [InputFieldComponent]
    });

    // create component and test fixture
    fixture = TestBed.createComponent(InputFieldComponent);

    // get test component from the fixture
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('form valid when empty', () => {
    expect(component.form.valid).toBeTruthy();
  });

  it('input field validity', () => {
    let errors = {};
    const inputTest = component.form.controls['testField'];
    expect(inputTest.valid).toBeTruthy();

    // input field is not required
    errors = inputTest.errors || {};
    expect(errors['required']).toBeFalsy();

    // Set input to min uncorrect
    inputTest.setValue('test');
    errors = inputTest.errors || {};
    expect(errors['minlength']).toBeTruthy();

    // Set input to min correct
    inputTest.setValue('testing');
    errors = inputTest.errors || {};
    expect(errors['minlength']).toBeFalsy();

    // Set input to max uncorrect
    inputTest.setValue('testing testing testing');
    errors = inputTest.errors || {};
    expect(errors['maxlength']).toBeTruthy();

    // Set input to max correct
    inputTest.setValue('testing');
    errors = inputTest.errors || {};
    expect(errors['maxlength']).toBeFalsy();
  });

});
